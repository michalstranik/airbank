package com.msd.mvparchitecture.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.model.transactions.Item;

public class FillUtils {

    @SuppressLint("SetTextI18n")
    public static void fillItem(Context context, Item item, ImageView typeImageView, TextView amountTextView, TextView directionTextView) {
        final int image = item.getDirection().equals(Item.Direction.INCOMING) ? R.drawable.ic_transaction_incoming : R.drawable.ic_transaction_outgoing;
        typeImageView.setImageDrawable(ContextCompat.getDrawable(context, image));
        amountTextView.setText(item.getAmountInAccountCurrency() + " " + context.getString(R.string.currency_czk));//seems to be something missing in backend data, therefore added manually
        directionTextView.setText(item.getDirection().name().toLowerCase());
    }

}

package com.msd.mvparchitecture.util;

import android.util.Log;

import com.msd.mvparchitecture.BuildConfig;

public class LogUtils {

    public static void d(String className, String method, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(className, method + " : " + message);
        }
    }

    public static void e(String className, String method, String message) {
        if (BuildConfig.DEBUG) {
            Log.e(className, method + " : " + message);
        }
    }

    public static void e(String className, String method, Exception exception) {
        if (BuildConfig.DEBUG) {
            Log.e(className, method + " : " + exception.toString());
        }
    }

}

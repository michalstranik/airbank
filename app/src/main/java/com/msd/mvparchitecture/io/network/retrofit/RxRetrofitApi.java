package com.msd.mvparchitecture.io.network.retrofit;

import com.msd.mvparchitecture.mvp.model.transaction.Transaction;
import com.msd.mvparchitecture.mvp.model.transactions.Transactions;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RxRetrofitApi {

    String baseUrl = "http://demo0569565.mockable.io/";

    @GET(baseUrl + "transactions")
    Observable<Transactions> getTransactions();

    @GET(baseUrl + "transactions/{id}")
    Observable<Transaction> getTransaction(@Path("id") int id);

}

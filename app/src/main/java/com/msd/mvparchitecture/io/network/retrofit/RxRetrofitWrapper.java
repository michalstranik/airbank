package com.msd.mvparchitecture.io.network.retrofit;

import com.msd.mvparchitecture.mvp.model.BaseResponse;

import java.net.SocketTimeoutException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

public abstract class RxRetrofitWrapper<RESPONSE extends BaseResponse> extends DisposableObserver<RESPONSE> {

    @Override
    public void onNext(RESPONSE response) {
        onSuccess(response);
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            final String code = Integer.toString(((HttpException) throwable).code());
            final String message = ((HttpException) throwable).message();
            onError(code + " " + message);
        } else if (throwable instanceof SocketTimeoutException) {
            onTimeout();
        } else {
            onError(throwable.getMessage());
        }
    }

    @Override
    public void onComplete() {

    }

    protected abstract void onSuccess(RESPONSE response);

    protected abstract void onTimeout();

    protected abstract void onError(String error);

}

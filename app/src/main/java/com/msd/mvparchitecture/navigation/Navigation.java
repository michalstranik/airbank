package com.msd.mvparchitecture.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.model.transactions.Item;
import com.msd.mvparchitecture.mvp.view.activity.BaseActivity;
import com.msd.mvparchitecture.mvp.view.fragment.DetailFragment;
import com.msd.mvparchitecture.mvp.view.fragment.MainFragment;

import java.util.List;

public class Navigation {

    public static final String ARG_ITEM = "ARG_ITEM";
    private static final int CONTAINER = R.id.fragment_container;

    private static boolean isFragmentExists(BaseActivity activity) {
        List<Fragment> fragmentList = activity.getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragmentList) {
            if (fragment instanceof MainFragment) {
                return true;
            }
        }
        return false;
    }

    public static void showMainFragment(BaseActivity activity) {
        final MainFragment fragment = new MainFragment();
        if (isFragmentExists(activity)) {
            return;
        }
        final FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(CONTAINER, fragment);
        transaction.commit();
    }

    public static void showDetailFragment(BaseActivity activity, Item item) {
        final DetailFragment fragment = DetailFragment.newInstance(item);
        final FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(CONTAINER, fragment).addToBackStack(null);
        transaction.commit();
    }

}

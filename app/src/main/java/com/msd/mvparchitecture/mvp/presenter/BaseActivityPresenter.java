package com.msd.mvparchitecture.mvp.presenter;

import com.msd.mvparchitecture.mvp.contract.BaseActivityContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;

public abstract class BaseActivityPresenter<MODEL extends BaseModel, VIEW extends BaseActivityContract.BaseActivityView> extends BasePresenter<MODEL, VIEW>
        implements BaseActivityContract.BaseActivityPresenter<MODEL> {

    protected BaseActivityPresenter(VIEW view) {
        super(view);
    }
}

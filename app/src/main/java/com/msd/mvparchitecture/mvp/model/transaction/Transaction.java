package com.msd.mvparchitecture.mvp.model.transaction;

import com.msd.mvparchitecture.mvp.model.BaseResponse;

public class Transaction extends BaseResponse {

    private ContraAccount contraAccount;

    public ContraAccount getContraAccount() {
        return contraAccount;
    }
}

package com.msd.mvparchitecture.mvp.presenter.fragment;

import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.io.network.retrofit.RxRetrofitWrapper;
import com.msd.mvparchitecture.mvp.contract.fragment.MainFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.model.transactions.Item;
import com.msd.mvparchitecture.mvp.model.transactions.Transactions;
import com.msd.mvparchitecture.mvp.presenter.BaseFragmentPresenter;
import com.msd.mvparchitecture.mvp.view.adapter.MainAdapter;
import com.msd.mvparchitecture.navigation.Navigation;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainFragmentPresenter extends BaseFragmentPresenter<MainFragmentPresenter.Model, MainFragmentContract.MainFragmentView>
        implements MainFragmentContract.MainFragmentPresenter<MainFragmentPresenter.Model> {

    private static final String TAG = "MainFragmentPresenter";
    private MainAdapter mainAdapter;

    public MainFragmentPresenter(MainFragmentContract.MainFragmentView view) {
        super(view);
    }

    @Override
    public void presenterOnCreatedView() {
        super.presenterOnCreatedView();
        initExpandableFilter();
        initFilter();
    }

    @Override
    protected boolean isModelNull() {
        return model.getTransactions() == null;
    }

    private void initExpandableFilter() {
        getView().getFilterRelativeLayout().setOnClickListener(v -> {
            if (getView().getFilterExpandableLayout().isExpanded()) {
                getView().getFilterImageView().setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_keyboard_arrow_down));
                getView().getFilterExpandableLayout().collapse();
            } else {
                getView().getFilterImageView().setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_keyboard_arrow_up));
                getView().getFilterExpandableLayout().expand();
            }
        });
    }

    private void initFilter() {
        getView().getBothRadioButton().setOnClickListener(v -> changeFilter(Item.Direction.BOTH));
        getView().getIncomingRadioButton().setOnClickListener(v -> changeFilter(Item.Direction.INCOMING));
        getView().getOutgoingRadioButton().setOnClickListener(v -> changeFilter(Item.Direction.OUTGOING));
    }

    private void changeFilter(Item.Direction direction) {
        model.setDirection(direction);
        updateView();
    }

    protected void callEndpoint() {
        final String method = "callEndpoint";
        //noinspection unchecked
        getDisposables().add(getRxRetrofitApi()
                .getTransactions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new RxRetrofitWrapper<Transactions>() {
                    @Override
                    protected void onSuccess(Transactions transactions) {
                        model.setTransactions(transactions);
                        handleSuccess(TAG, method);
                    }

                    @Override
                    protected void onTimeout() {
                        handleTimeout(TAG, method);
                    }

                    @Override
                    protected void onError(String error) {
                        handleError(TAG, method, error);
                    }
                }));
    }

    protected void updateView() {
        if (model.getTransactions() == null) {
            return;
        }
        model.setDirection(model.getDirection());

        //noinspection unchecked
        getDisposables().add(Observable
                .fromArray(model.getTransactions().getItems())
                .subscribeOn(Schedulers.io())
                .flatMapIterable((Function<List<Item>, List<Item>>) items -> items)
                .filter(item -> {
                    switch (model.getDirection()) {
                        case BOTH:
                            return true;

                        default:
                            return model.getDirection().equals(item.getDirection());
                    }
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    final Transactions transactions = new Transactions();
                    transactions.setItems(items);
                    updateViewAdapter(transactions);
                }));
    }

    private void updateViewAdapter(Transactions transactions) {
        if (model.getTransactions() == null) {
            return;
        }
        if (mainAdapter == null) {
            mainAdapter = new MainAdapter(transactions, item -> Navigation.showDetailFragment(getView().getBaseActivity(), item));
            final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            getView().getRecyclerView().setLayoutManager(layoutManager);
            getView().getRecyclerView().addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
            getView().getRecyclerView().setAdapter(mainAdapter);
            final Parcelable recyclerViewState = getView().getRecyclerViewState();
            if (recyclerViewState != null) {
                getView().getRecyclerView().getLayoutManager().onRestoreInstanceState(recyclerViewState);
            }
        } else {
            mainAdapter.update(transactions);
        }
    }

    @Override
    public Model getModelToSave() {
        return model;
    }

    @Override
    public void restoreModel(Model model) {
        this.model = model;
    }

    @Override
    public void createModel() {
        model = new Model();
    }

    public static class Model extends BaseModel {
        private Transactions transactions;
        private Item.Direction direction = Item.Direction.BOTH;//default

        Transactions getTransactions() {
            return transactions;
        }

        void setTransactions(Transactions transactions) {
            this.transactions = transactions;
        }

        Item.Direction getDirection() {
            return direction;
        }

        void setDirection(Item.Direction direction) {
            this.direction = direction;
        }
    }

}
package com.msd.mvparchitecture.mvp.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.BaseActivityContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.view.dialog.ProgressDialogFragment;
import com.msd.mvparchitecture.mvp.view.fragment.BaseFragment;
import com.msd.mvparchitecture.util.InternetUtils;

import butterknife.ButterKnife;

public abstract class BaseActivity<MODEL extends BaseModel, PRESENTER extends BaseActivityContract.BasePresenter> extends AppCompatActivity implements BaseActivityContract.BaseView {

    private static final String MODEL_STATE = "ACTIVITY_STATE";
    private static final String PROGRESS_DIALOG_TAG = "PROGRESS_DIALOG_TAG";
    private PRESENTER presenter;
    private boolean initLayoutDone = false;
    private ImageView actionBarBackImageView;
    private TextView actionBarTitleTextView, actionBarSubtitleTextView;

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        restoreModel(savedInstanceState);
        presenter.presenterOnCreate();
        initLayout();
        binViews();
        setCustomActionBar();
        updateTitle();
    }

    @CallSuper
    @Override
    protected void onStart() {
        super.onStart();
        if (!initLayoutDone) {
            onCreatedView();
            presenter.presenterOnCreatedView();
            initLayoutDone = true;
        }
        presenter.presenterOnStart();
    }

    @CallSuper
    @Override
    protected void onResume() {
        super.onResume();
        presenter.presenterOnResume();
    }

    @CallSuper
    @Override
    protected void onPause() {
        super.onPause();
        presenter.presenterOnPause();
    }

    @CallSuper
    @Override
    protected void onStop() {
        super.onStop();
        presenter.presenterOnStop();
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.presenterOnDestroy();
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final Bundle bundle = getModelToSave();
        if (bundle != null) {
            outState.putAll(bundle);
        }
    }

    @SuppressWarnings("unchecked")
    private void restoreModel(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(MODEL_STATE)) {
            final MODEL serializable = (MODEL) savedInstanceState.getSerializable(MODEL_STATE);
            if (serializable != null) getPresenter().restoreModel(serializable);
        }
    }

    @SuppressWarnings("unchecked")
    private Bundle getModelToSave() {
        Bundle bundle = new Bundle();
        final MODEL modelToSave = (MODEL) getPresenter().getModelToSave();
        if (modelToSave != null) {
            bundle.putSerializable(MODEL_STATE, modelToSave);
        }
        return bundle;
    }

    @Override
    public Context getContext() {
        return this;
    }

    protected abstract void initLayout();

    private void binViews() {
        ButterKnife.bind(this);
    }

    /**
     * CALL WHEN VIEW IS READY (LAYOUT + BINDING)
     */
    protected abstract void onCreatedView();

    protected abstract PRESENTER createPresenter();

    private PRESENTER getPresenter() {
        return presenter;
    }

    @CallSuper
    @Override
    public void showProgressDialog() {
        if (!InternetUtils.isNetworkAvailable(getContext())) {
            return;
        }

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            if (fragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG) == null) {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ProgressDialogFragment progressDialog = new ProgressDialogFragment();
                progressDialog.show(ft, PROGRESS_DIALOG_TAG);
            }
        }
    }

    @CallSuper
    @Override
    public void hideProgressDialog() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            final ProgressDialogFragment progressDialog = (ProgressDialogFragment) fragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG);
            if (progressDialog != null) {
                progressDialog.dismissAllowingStateLoss();
            }
        }
    }

    @CallSuper
    @Override
    public void setToolbarTitle(String title) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && actionBarTitleTextView != null) {
            actionBarTitleTextView.setText(title);
            actionBar.setElevation(0);
            if (actionBarSubtitleTextView != null) {
                actionBarSubtitleTextView.setVisibility(View.GONE);
            }
        }
    }

    @CallSuper
    @Override
    public void setToolbarSubTitle(String subTitle) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && actionBarSubtitleTextView != null) {
            actionBarSubtitleTextView.setText(subTitle);
            actionBarSubtitleTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setToolbarDisplayHomeAsUpEnabled(boolean value) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && actionBarBackImageView != null) {
            actionBarBackImageView.setVisibility(value ? View.VISIBLE : View.GONE);
        }
    }

    private void setCustomActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.action_bar);
            actionBarBackImageView = findViewById(getResources().getIdentifier("actionBarBackImageView", "id", getPackageName()));
            actionBarTitleTextView = findViewById(getResources().getIdentifier("actionBarTitleTextView", "id", getPackageName()));
            actionBarSubtitleTextView = findViewById(getResources().getIdentifier("actionBarSubtitleTextView", "id", getPackageName()));
            actionBarBackImageView.setOnClickListener(v -> onBackPressed());
        }
    }

    private void updateTitle() {
        getSupportFragmentManager().addOnBackStackChangedListener(
                () -> {
                    final BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    fragment.updateToolbar();
                });
    }

}

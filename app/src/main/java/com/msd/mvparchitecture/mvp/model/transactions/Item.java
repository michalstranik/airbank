package com.msd.mvparchitecture.mvp.model.transactions;

import com.msd.mvparchitecture.mvp.model.BaseResponse;

public class Item extends BaseResponse {

    public enum Direction{
        BOTH,
        INCOMING,
        OUTGOING
    }

    private Integer id;
    private Integer amountInAccountCurrency;
    private Direction direction;

    public Integer getId() {
        return id;
    }

    public Integer getAmountInAccountCurrency() {
        return amountInAccountCurrency;
    }

    public Direction getDirection() {
        return direction;
    }
}

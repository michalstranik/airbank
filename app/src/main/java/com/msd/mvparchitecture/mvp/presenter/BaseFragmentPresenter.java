package com.msd.mvparchitecture.mvp.presenter;

import android.support.annotation.CallSuper;
import android.view.View;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.BaseFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.util.InternetUtils;
import com.msd.mvparchitecture.util.LogUtils;

public abstract class BaseFragmentPresenter<MODEL extends BaseModel, VIEW extends BaseFragmentContract.BaseFragmentView> extends BasePresenter<MODEL, VIEW>
        implements BaseFragmentContract.BaseFragmentPresenter<MODEL> {

    protected BaseFragmentPresenter(VIEW view) {
        super(view);
    }

    @CallSuper
    @Override
    public void presenterOnStart() {
        super.presenterOnStart();
        if (InternetUtils.isNetworkAvailable(getContext())) {
            if (isModelNull()) {
                getView().showProgressDialog();
            } else {
                updateView();
            }
            callEndpoint();
        } else {
            getView().getRetryErrorMessageTextView().setText(getString(R.string.error_internet));
            showRetry(true);
        }
    }

    protected abstract boolean isModelNull();

    @CallSuper
    @Override
    public void presenterRetry() {
        super.presenterRetry();
        if (InternetUtils.isNetworkAvailable(getContext())) {
            getView().showProgressDialog();
            callEndpoint();
        } else {
            getView().getRetryErrorMessageTextView().setText(getString(R.string.error_internet));
        }
    }

    private void showRetry(boolean show) {
        getView().getRetryLinearLayout().setVisibility(show ? View.VISIBLE : View.GONE);
        getView().getHideOnRetryLinearLayout().setVisibility(show ? View.GONE : View.VISIBLE);
    }

    protected abstract void callEndpoint();

    protected void handleSuccess(String TAG, String method) {
        LogUtils.d(TAG, method, "onSuccess");
        showRetry(false);
        updateView();
        getView().hideProgressDialog();
    }

    protected void handleTimeout(String TAG, String method) {
        LogUtils.e(TAG, method, "onTimeout");
        getView().hideProgressDialog();
        getView().getRetryErrorMessageTextView().setText(getString(R.string.error_timeout));
        showRetry(true);
    }

    protected void handleError(String TAG, String method, String error) {
        LogUtils.e(TAG, method, "onError = " + error);
        getView().hideProgressDialog();
        getView().getRetryErrorMessageTextView().setText(error);
        showRetry(true);
    }

    protected abstract void updateView();

}

package com.msd.mvparchitecture.mvp.model.transaction;

import com.msd.mvparchitecture.mvp.model.BaseResponse;

public class ContraAccount extends BaseResponse {

    private String accountNumber;
    private String accountName;
    private String bankCode;

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getBankCode() {
        return bankCode;
    }
}

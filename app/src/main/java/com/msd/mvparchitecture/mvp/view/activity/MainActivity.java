package com.msd.mvparchitecture.mvp.view.activity;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.activity.MainActivityContract;
import com.msd.mvparchitecture.mvp.presenter.activity.MainActivityPresenter;
import com.msd.mvparchitecture.navigation.Navigation;

public class MainActivity extends BaseActivity<MainActivityPresenter.Model, MainActivityPresenter> implements MainActivityContract.MainActivityView {

    @Override
    protected void initLayout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onCreatedView() {
        Navigation.showMainFragment(this);
    }

    @Override
    protected MainActivityPresenter createPresenter() {
        return new MainActivityPresenter(this);
    }

}


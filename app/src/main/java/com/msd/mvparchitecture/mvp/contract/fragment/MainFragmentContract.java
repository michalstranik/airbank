package com.msd.mvparchitecture.mvp.contract.fragment;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.msd.mvparchitecture.mvp.contract.BaseFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;

import net.cachapa.expandablelayout.ExpandableLayout;

public interface MainFragmentContract {

    interface MainFragmentPresenter<MODEL extends BaseModel> extends BaseFragmentContract.BaseFragmentPresenter<MODEL> {
    }

    interface MainFragmentView extends BaseFragmentContract.BaseFragmentView {

        RelativeLayout getFilterRelativeLayout();

        ExpandableLayout getFilterExpandableLayout();

        ImageView getFilterImageView();

        RadioButton getBothRadioButton();

        RadioButton getIncomingRadioButton();

        RadioButton getOutgoingRadioButton();

        RecyclerView getRecyclerView();

    }

}
package com.msd.mvparchitecture.mvp.model.transactions;

import com.msd.mvparchitecture.mvp.model.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class Transactions extends BaseResponse {

    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}

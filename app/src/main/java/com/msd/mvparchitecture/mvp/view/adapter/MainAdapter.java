package com.msd.mvparchitecture.mvp.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.model.transactions.Item;
import com.msd.mvparchitecture.mvp.model.transactions.Transactions;
import com.msd.mvparchitecture.util.FillUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Item> list;
    private final Callback callback;

    public interface Callback {
        void onItemClick(Item item);
    }

    public MainAdapter(Transactions transactions, Callback callback) {
        this.list = transactions.getItems();
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_transaction, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final Context context = viewHolder.itemView.getContext();
        final Item item = list.get(position);

        FillUtils.fillItem(context, item, viewHolder.typeImageView, viewHolder.amountTextView, viewHolder.directionTextView);

        viewHolder.itemView.setOnClickListener(v -> callback.onItemClick(item));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void update(Transactions transactions) {
        this.list = transactions.getItems();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.typeImageView)
        ImageView typeImageView;
        @BindView(R.id.amountTextView)
        TextView amountTextView;
        @BindView(R.id.directionTextView)
        TextView directionTextView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

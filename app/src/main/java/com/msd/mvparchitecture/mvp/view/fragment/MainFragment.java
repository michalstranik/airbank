package com.msd.mvparchitecture.mvp.view.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.fragment.MainFragmentContract;
import com.msd.mvparchitecture.mvp.presenter.fragment.MainFragmentPresenter;

import net.cachapa.expandablelayout.ExpandableLayout;

import butterknife.BindView;

public class MainFragment extends BaseFragment<MainFragmentPresenter.Model, MainFragmentPresenter> implements MainFragmentContract.MainFragmentView {

    @BindView(R.id.filterRelativeLayout)
    RelativeLayout filterRelativeLayout;
    @BindView(R.id.filterExpandableLayout)
    ExpandableLayout filterExpandableLayout;
    @BindView(R.id.filterImageView)
    ImageView filterImageView;
    @BindView(R.id.bothRadioButton)
    RadioButton bothRadioButton;
    @BindView(R.id.incomingRadioButton)
    RadioButton incomingRadioButton;
    @BindView(R.id.outgoingRadioButton)
    RadioButton outgoingRadioButton;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected MainFragmentPresenter createPresenter() {
        return new MainFragmentPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateToolbar();
    }

    @Override
    public void updateToolbar() {
        setToolbarDisplayHomeAsUpEnabled(false);
        setToolbarTitle(getString(R.string.list));
    }

    @Override
    public RelativeLayout getFilterRelativeLayout() {
        return filterRelativeLayout;
    }

    @Override
    public ExpandableLayout getFilterExpandableLayout() {
        return filterExpandableLayout;
    }

    @Override
    public ImageView getFilterImageView() {
        return filterImageView;
    }

    @Override
    public RadioButton getBothRadioButton() {
        return bothRadioButton;
    }

    @Override
    public RadioButton getIncomingRadioButton() {
        return incomingRadioButton;
    }

    @Override
    public RadioButton getOutgoingRadioButton() {
        return outgoingRadioButton;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}

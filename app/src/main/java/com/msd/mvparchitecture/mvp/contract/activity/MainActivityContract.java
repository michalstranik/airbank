package com.msd.mvparchitecture.mvp.contract.activity;

import com.msd.mvparchitecture.mvp.contract.BaseActivityContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;

public interface MainActivityContract {

    interface MainActivityPresenter<MODEL extends BaseModel> extends BaseActivityContract.BaseActivityPresenter<MODEL> {
    }

    interface MainActivityView extends BaseActivityContract.BaseActivityView {
    }

}

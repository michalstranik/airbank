package com.msd.mvparchitecture.mvp.presenter.fragment;

import android.view.View;

import com.msd.mvparchitecture.io.network.retrofit.RxRetrofitWrapper;
import com.msd.mvparchitecture.mvp.contract.fragment.DetailFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.model.transaction.Transaction;
import com.msd.mvparchitecture.mvp.model.transactions.Item;
import com.msd.mvparchitecture.mvp.presenter.BaseFragmentPresenter;
import com.msd.mvparchitecture.util.FillUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DetailFragmentPresenter extends BaseFragmentPresenter<DetailFragmentPresenter.Model, DetailFragmentContract.DetailFragmentView>
        implements DetailFragmentContract.DetailFragmentPresenter<DetailFragmentPresenter.Model> {

    private static final String TAG = "DetailFragmentPresenter";

    public DetailFragmentPresenter(DetailFragmentContract.DetailFragmentView view) {
        super(view);
    }

    @Override
    public void presenterOnCreatedView() {
        super.presenterOnCreatedView();
        getView().getArrowImageView().setVisibility(View.GONE);
        FillUtils.fillItem(getContext(), model.getItem(), getView().getTypeImageView(), getView().getAmountTextView(), getView().getDirectionTextView());
    }

    @Override
    protected boolean isModelNull() {
        return model.getTransaction() == null;
    }

    protected void callEndpoint() {
        final String method = "callEndpoint";
        //noinspection unchecked
        getDisposables().add(getRxRetrofitApi()
                .getTransaction(model.getItem().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new RxRetrofitWrapper<Transaction>() {
                    @Override
                    protected void onSuccess(Transaction transaction) {
                        model.setTransaction(transaction);
                        handleSuccess(TAG, method);
                    }

                    @Override
                    protected void onTimeout() {
                        handleTimeout(TAG, method);
                    }

                    @Override
                    protected void onError(String error) {
                        handleError(TAG, method, error);
                    }
                }));
    }

    protected void updateView() {
        getView().getAccountNumberTextView().setText(model.getTransaction().getContraAccount().getAccountNumber());
        getView().getAccountNameTextView().setText(model.getTransaction().getContraAccount().getAccountName());
        getView().getBankCodeTextView().setText(model.getTransaction().getContraAccount().getBankCode());
        getView().getHideOnRetryLinearLayout().setVisibility(View.VISIBLE);
    }

    @Override
    public void setItem(Item item) {
        model.setItem(item);
    }

    @Override
    public Model getModelToSave() {
        return model;
    }

    @Override
    public void restoreModel(Model model) {
        this.model = model;
    }

    @Override
    public void createModel() {
        model = new Model();
    }

    public static class Model extends BaseModel {
        private Item item;
        private Transaction transaction;

        Item getItem() {
            return item;
        }

        void setItem(Item item) {
            this.item = item;
        }

        Transaction getTransaction() {
            return transaction;
        }

        void setTransaction(Transaction transaction) {
            this.transaction = transaction;
        }
    }

}
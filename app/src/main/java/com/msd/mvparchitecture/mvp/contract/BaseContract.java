package com.msd.mvparchitecture.mvp.contract;


import android.content.Context;

import com.msd.mvparchitecture.mvp.model.BaseModel;

public interface BaseContract {

    interface BasePresenter<MODEL extends BaseModel> {

        void presenterOnCreate();

        void presenterOnCreatedView();

        void presenterOnStart();

        void presenterOnResume();

        void presenterOnPause();

        void presenterOnStop();

        void presenterOnDestroy();

        void presenterRetry();

        MODEL getModelToSave();

        void restoreModel(MODEL model);

        void createModel();

    }

    interface BaseView {

        Context getContext();

        void showProgressDialog();

        void hideProgressDialog();

        void setToolbarTitle(String title);

        void setToolbarSubTitle(String subTitle);

        void setToolbarDisplayHomeAsUpEnabled(boolean value);

    }

}

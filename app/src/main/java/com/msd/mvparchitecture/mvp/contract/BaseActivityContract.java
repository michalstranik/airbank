package com.msd.mvparchitecture.mvp.contract;

import com.msd.mvparchitecture.mvp.model.BaseModel;

public interface BaseActivityContract extends BaseContract {

    interface BaseActivityPresenter<MODEL extends BaseModel> extends BaseContract.BasePresenter<MODEL> {
    }

    interface BaseActivityView extends BaseContract.BaseView {
    }

}

package com.msd.mvparchitecture.mvp.view.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.BaseFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.view.activity.BaseActivity;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class BaseFragment<MODEL extends BaseModel, PRESENTER extends BaseFragmentContract.BaseFragmentPresenter> extends DialogFragment implements BaseFragmentContract.BaseFragmentView {

    private static final String MODEL_STATE = "FRAGMENT_STATE";
    private static final String STATE_ADAPTER = "STATE_ADAPTER";
    private PRESENTER presenter;
    private boolean initLayoutDone = false;
    private Parcelable recyclerViewState;

    @BindView(R.id.hideOnRetryLinearLayout)
    LinearLayout hideOnRetryLinearLayout;
    @BindView(R.id.retryLinearLayout)
    LinearLayout retryLinearLayout;
    @BindView(R.id.retryErrorMessageTextView)
    TextView retryErrorMessageTextView;

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        restoreModel(savedInstanceState);
        presenter.presenterOnCreate();
    }

    @CallSuper
    @Override
    public void onStart() {
        super.onStart();
        if (!initLayoutDone) {
            binViews();
            presenter.presenterOnCreatedView();
            initLayoutDone = true;
        }
        presenter.presenterOnStart();
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        presenter.presenterOnResume();
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        presenter.presenterOnPause();
    }

    @CallSuper
    @Override
    public void onStop() {
        super.onStop();
        presenter.presenterOnStop();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.presenterOnDestroy();
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final Bundle bundle = getModelToSave();
        if (bundle != null) {
            outState.putAll(bundle);
        }
    }

    @SuppressWarnings("unchecked")
    private void restoreModel(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            recyclerViewState = savedInstanceState.getParcelable(STATE_ADAPTER);
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(MODEL_STATE)) {
            final MODEL serializable = (MODEL) savedInstanceState.getSerializable(MODEL_STATE);
            if (serializable != null) getPresenter().restoreModel(serializable);
        }
    }

    @SuppressWarnings("unchecked")
    private Bundle getModelToSave() {
        final Bundle bundle = new Bundle();

        if (getRecyclerView() != null) {
            bundle.putParcelable(STATE_ADAPTER, getRecyclerView().getLayoutManager().onSaveInstanceState());
        }

        final MODEL modelToSave = (MODEL) getPresenter().getModelToSave();
        if (modelToSave != null) {
            bundle.putSerializable(MODEL_STATE, modelToSave);
        }
        return bundle;
    }

    protected abstract RecyclerView getRecyclerView();

    public Parcelable getRecyclerViewState() {
        return recyclerViewState;
    }

    private void binViews() {
        ButterKnife.bind(this, Objects.requireNonNull(getView()));
    }

    protected abstract PRESENTER createPresenter();

    PRESENTER getPresenter() {
        return presenter;
    }

    public abstract void updateToolbar();

    @CallSuper
    @Override
    public void showProgressDialog() {
        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgressDialog();
        }
    }

    @CallSuper
    @Override
    public void hideProgressDialog() {
        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgressDialog();
        }
    }

    @CallSuper
    @Override
    public void setToolbarTitle(String title) {
        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.setToolbarTitle(title);
        }
    }

    @CallSuper
    @Override
    public void setToolbarSubTitle(String subTitle) {
        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.setToolbarSubTitle(subTitle);
        }
    }

    @CallSuper
    @Override
    public void setToolbarDisplayHomeAsUpEnabled(boolean value) {
        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.setToolbarDisplayHomeAsUpEnabled(value);
        }
    }

    @OnClick(R.id.retryLinearLayout)
    void retry() {
        presenter.presenterRetry();
    }

    @Override
    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public LinearLayout getHideOnRetryLinearLayout() {
        return hideOnRetryLinearLayout;
    }

    @Override
    public LinearLayout getRetryLinearLayout() {
        return retryLinearLayout;
    }

    @Override
    public TextView getRetryErrorMessageTextView() {
        return retryErrorMessageTextView;
    }
}

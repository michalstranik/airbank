package com.msd.mvparchitecture.mvp.presenter;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.msd.mvparchitecture.app.App;
import com.msd.mvparchitecture.io.network.retrofit.RxRetrofitApi;
import com.msd.mvparchitecture.mvp.contract.BaseContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<MODEL extends BaseModel, VIEW extends BaseContract.BaseView> implements BaseContract.BasePresenter<MODEL> {

    protected MODEL model;
    private final VIEW view;
    private final CompositeDisposable disposables;
    private final RxRetrofitApi rxRetrofitApi;

    BasePresenter(VIEW view) {
        this.view = view;
        this.disposables = new CompositeDisposable();
        this.rxRetrofitApi = App.getInstance().getRxRetrofitApi();
        createModel();
    }

    @CallSuper
    @Override
    public void presenterOnCreate() {
    }

    @CallSuper
    @Override
    public void presenterOnCreatedView() {
    }

    @CallSuper
    @Override
    public void presenterOnStart() {
    }

    @CallSuper
    @Override
    public void presenterOnResume() {
    }

    @CallSuper
    @Override
    public void presenterOnPause() {
    }

    @CallSuper
    @Override
    public void presenterOnStop() {
    }

    @CallSuper
    @Override
    public void presenterRetry() {
    }

    @CallSuper
    @Override
    public void presenterOnDestroy() {
        disposables.dispose();
    }

    protected VIEW getView() {
        return view;
    }

    protected CompositeDisposable getDisposables() {
        return disposables;
    }

    protected RxRetrofitApi getRxRetrofitApi() {
        return rxRetrofitApi;
    }

    @Override
    public MODEL getModelToSave() {
        return null;
    }

    @Override
    public void restoreModel(MODEL model) {
    }

    @Override
    public void createModel() {
    }

    protected Context getContext() {
        return getView().getContext();
    }

    String getString(int stringId) {
        return getView().getContext().getString(stringId);
    }
}

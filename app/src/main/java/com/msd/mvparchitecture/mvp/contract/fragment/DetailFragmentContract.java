package com.msd.mvparchitecture.mvp.contract.fragment;

import android.widget.ImageView;
import android.widget.TextView;

import com.msd.mvparchitecture.mvp.contract.BaseFragmentContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.model.transactions.Item;

public interface DetailFragmentContract {

    interface DetailFragmentPresenter<MODEL extends BaseModel> extends BaseFragmentContract.BaseFragmentPresenter<MODEL> {

        void setItem(Item item);

    }

    interface DetailFragmentView extends BaseFragmentContract.BaseFragmentView {

        ImageView getTypeImageView();

        TextView getAmountTextView();

        TextView getDirectionTextView();

        ImageView getArrowImageView();

        TextView getAccountNumberTextView();

        TextView getAccountNameTextView();

        TextView getBankCodeTextView();

    }

}
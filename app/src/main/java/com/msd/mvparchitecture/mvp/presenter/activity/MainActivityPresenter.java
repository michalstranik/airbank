package com.msd.mvparchitecture.mvp.presenter.activity;

import com.msd.mvparchitecture.mvp.contract.activity.MainActivityContract;
import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.presenter.BaseActivityPresenter;

public class MainActivityPresenter extends BaseActivityPresenter<MainActivityPresenter.Model, MainActivityContract.MainActivityView>
        implements MainActivityContract.MainActivityPresenter<MainActivityPresenter.Model> {

    public MainActivityPresenter(MainActivityContract.MainActivityView view) {
        super(view);
    }

    public static class Model extends BaseModel {

    }

}
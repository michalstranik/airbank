package com.msd.mvparchitecture.mvp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msd.mvparchitecture.R;
import com.msd.mvparchitecture.mvp.contract.fragment.DetailFragmentContract;
import com.msd.mvparchitecture.mvp.model.transactions.Item;
import com.msd.mvparchitecture.mvp.presenter.fragment.DetailFragmentPresenter;
import com.msd.mvparchitecture.navigation.Navigation;

import butterknife.BindView;

public class DetailFragment extends BaseFragment<DetailFragmentPresenter.Model, DetailFragmentPresenter> implements DetailFragmentContract.DetailFragmentView {

    @BindView(R.id.typeImageView)
    ImageView typeImageView;
    @BindView(R.id.amountTextView)
    TextView amountTextView;
    @BindView(R.id.directionTextView)
    TextView directionTextView;
    @BindView(R.id.arrowImageView)
    ImageView arrowImageView;
    @BindView(R.id.accountNumberTextView)
    TextView accountNumberTextView;
    @BindView(R.id.accountNameTextView)
    TextView accountNameTextView;
    @BindView(R.id.bankCodeTextView)
    TextView bankCodeTextView;

    public static DetailFragment newInstance(Item item) {
        final DetailFragment fragment = new DetailFragment();
        final Bundle args = new Bundle();
        args.putSerializable(Navigation.ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected DetailFragmentPresenter createPresenter() {
        return new DetailFragmentPresenter(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            final Item item = (Item) args.getSerializable(Navigation.ARG_ITEM);
            getPresenter().setItem(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateToolbar();
    }

    @Override
    public void updateToolbar() {
        setToolbarDisplayHomeAsUpEnabled(true);
        setToolbarTitle(getString(R.string.detail));
    }

    @Override
    public ImageView getTypeImageView() {
        return typeImageView;
    }

    @Override
    public TextView getAmountTextView() {
        return amountTextView;
    }

    @Override
    public TextView getDirectionTextView() {
        return directionTextView;
    }

    @Override
    public ImageView getArrowImageView() {
        return arrowImageView;
    }

    @Override
    public TextView getAccountNumberTextView() {
        return accountNumberTextView;
    }

    @Override
    public TextView getAccountNameTextView() {
        return accountNameTextView;
    }

    @Override
    public TextView getBankCodeTextView() {
        return bankCodeTextView;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return null;
    }
}

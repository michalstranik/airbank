package com.msd.mvparchitecture.mvp.contract;

import android.os.Parcelable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.msd.mvparchitecture.mvp.model.BaseModel;
import com.msd.mvparchitecture.mvp.view.activity.BaseActivity;

public interface BaseFragmentContract extends BaseContract {

    interface BaseFragmentPresenter<MODEL extends BaseModel> extends BaseContract.BasePresenter<MODEL> {
    }

    interface BaseFragmentView extends BaseContract.BaseView {

        BaseActivity getBaseActivity();

        Parcelable getRecyclerViewState();

        LinearLayout getHideOnRetryLinearLayout();

        LinearLayout getRetryLinearLayout();

        TextView getRetryErrorMessageTextView();

    }

}

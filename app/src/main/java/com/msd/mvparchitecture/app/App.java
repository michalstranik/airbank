package com.msd.mvparchitecture.app;

import android.app.Application;

import com.msd.mvparchitecture.io.network.retrofit.RxRetrofitApi;
import com.msd.mvparchitecture.io.network.retrofit.RxRetrofitClient;

public class App extends Application {

    private static App instance;
    private RxRetrofitApi rxRetrofitApi;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        rxRetrofitApi = RxRetrofitClient.get().create(RxRetrofitApi.class);
    }

    public static App getInstance() {
        return instance;
    }

    public RxRetrofitApi getRxRetrofitApi() {
        return rxRetrofitApi;
    }
}
